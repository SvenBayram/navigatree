﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavigaTree
{
    public enum TreeItemType
    {
        File,
        Dir,
        Drive
    }

    public class TreeItem
    {
        private long usedSpace;
        private long totalSpace;

        public string ParentFolder { get; set; }
        public string Titel { get; set; }
        public string Datatype { get; set; }
        public string Path { get; set; }
        public long UsedSpaceLong { get => usedSpace; set => usedSpace = value; }
        public string UsedSpace { get => usedSpace.FormatBytes(); }
        public long TotalSpaceLong { get => totalSpace; set => totalSpace = value; }
        public string TotalSpace { get => totalSpace.FormatBytes(); }
        public int PercentageSpace { get; set; }
        public TreeItemType Type { get; private set; }
        public ObservableCollection<TreeItem> SubItems { get; set; }

        public TreeItem(TreeItemType type)
        {
            Type = type;
            SubItems = new ObservableCollection<TreeItem>();
        }

        public void AddSubItem(TreeItem item)
        {
            App.Current.Dispatcher.Invoke(() => { SubItems.Add(item); });
        }
    }
}
