﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace NavigaTree
{
    class Indexer
    {
        private List<Crawler> CrawlerList = new List<Crawler>();
        private ObservableCollection<TreeItem> tree = new ObservableCollection<TreeItem>();

        public ObservableCollection<TreeItem> Tree { get => tree; set => tree = value; }

        public Indexer(List<DriveInfo> drives)
        { 
            foreach (DriveInfo drive in drives)
            {
                if (drive.DriveType == DriveType.Fixed || drive.DriveType == DriveType.Removable)
                {
                    CrawlerList.Add(new Crawler(drive.Name));
                    tree.Add(CrawlerList[CrawlerList.Count-1].Root);
                }
            }
        }

        public bool ActiveCrawler()
        {
            //Gibt irgendwas flasch zurück:
            //ToDo: Methode und Methodenaufrufe überprüfen!
            foreach (Crawler crawler in CrawlerList)
            {
                if (crawler.Active)
                {
                    return true;
                }
            }
            return false;
        }

        public void StopThreads()
        {
            for (int i = 0; i < CrawlerList.Count; i++)
            {
                CrawlerList[i].StopCrawler();
                CrawlerList[i] = null;
            }
        }

    }
}
