﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace NavigaTree
{
    class Crawler
    {
        private Thread crawlerThread;
        private TreeItem root;
        private Queue<TreeItem> outstanding;
        private bool active = true;

        public TreeItem Root { get => root; }
        public bool Active { get => active; set => active = value; }

        public Crawler(string drivePath)
        {
            DriveInfo driver = new DriveInfo(drivePath);
            App.Current.Dispatcher.Invoke(() =>
            {
                root = new TreeItem(TreeItemType.Drive)
                {
                    ParentFolder = "Root",
                    Titel = driver.Name + " (" + driver.VolumeLabel + ")" + " Size: " + driver.TotalSize.FormatBytes(),
                    Path = drivePath,
                    Datatype = driver.DriveType.ToString(),
                    TotalSpaceLong = driver.TotalSize
                };
            });
            outstanding = new Queue<TreeItem>();
            outstanding.Enqueue(root);

            crawlerThread = new Thread(Crawl);
            crawlerThread.Start();
        }


        private void Crawl()
        {
            TreeItem currentFolder;
            while (outstanding.Any())
            {
                currentFolder = outstanding.Dequeue();

                try
                {
                    foreach (DirectoryInfo info in new DirectoryInfo(currentFolder.Path).GetDirectories())
                    {
                        TreeItem treeItem = new TreeItem(TreeItemType.Dir)
                        {
                            //Add here infos that you want to show later in the treeview!
                            ParentFolder = currentFolder.Titel,
                            Titel = info.Name,
                            Path = info.FullName,
                            Datatype = "Folder"
                        };
                        currentFolder.AddSubItem(treeItem);
                        outstanding.Enqueue(treeItem);
                    }

                    foreach (FileInfo info in new DirectoryInfo(currentFolder.Path).GetFiles())
                    {
                        TreeItem treeItem = new TreeItem(TreeItemType.File)
                        {
                            //Add here infos that you want to show later in the treeview!
                            ParentFolder = currentFolder.Titel,
                            Titel = info.Name,
                            Path = info.FullName,
                            Datatype = info.Extension,
                            UsedSpaceLong = info.Length,
                            TotalSpaceLong = info.Length
                        };
                        currentFolder.AddSubItem(treeItem);
                    }
                }
                catch (UnauthorizedAccessException)
                {

                }
                catch (PathTooLongException)
                {

                }
                catch (Exception)
                {
                    throw;
                }
                Thread.Sleep(1);
            }

            root.UsedSpaceLong = CalculateSize(root);
            root.PercentageSpace = (int)((root.UsedSpaceLong * 100) / root.TotalSpaceLong) ;

            App.Current.Dispatcher.Invoke(() => 
            {
                ((MainWindow)App.Current.MainWindow).RootTree.Items.Refresh();
            });
                active = false;

        }

        public void StopCrawler()
        {
            active = false;
            crawlerThread.Abort();
        }

        private long CalculateSize(TreeItem treeItem)
        {
            if (treeItem.SubItems.Count == 0)
            {
                return treeItem.UsedSpaceLong;
            }
            
            foreach (TreeItem item in treeItem.SubItems)
            {
                if (item.Type == TreeItemType.Dir)
                {
                    item.UsedSpaceLong = CalculateSize(item);
                    item.TotalSpaceLong = item.UsedSpaceLong;
                }
                treeItem.UsedSpaceLong += item.UsedSpaceLong;
            }

            foreach (TreeItem item in treeItem.SubItems)
            {
                try
                {
                    item.PercentageSpace = (int)((item.UsedSpaceLong * 100) / treeItem.UsedSpaceLong);
                }
                catch (Exception)
                {
                }
            }
            return treeItem.UsedSpaceLong;
        }
    }
}
