﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NavigaTree
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Thread> treeBuilderThreads;
        bool active = true;
        List<DriveInfo> drives;

        public MainWindow()
        {
            InitializeComponent();
            treeBuilderThreads = new List<Thread>();
            CreateDrivesCombobox();
        }

        private void Builder()
        {
            Indexer indexer = new Indexer(drives);
            Dispatcher.Invoke(() => { RootTree.DataContext = indexer.Tree; });
            while (active)
            {
                //ToDo: Überprüfen. Wenn active = false; einkommentiert wird, brechen die Threads zu früh ab.

                if (indexer.ActiveCrawler())
                {
                    //ToDo: Status Check!
                }
                else
                {
                    //active = false;
                }
            }
            indexer.StopThreads();
        }

        private void CreateDrivesCombobox()
        {
            List<string> driverSelection = new List<string>();
            DriveSelection.DataContext = driverSelection;
            DriveInfo[] drives = DriveInfo.GetDrives();

            driverSelection.Add("All Drives");
            driverSelection.Add("Hard Drives");
            driverSelection.Add("Hard Drives + Removable Drives");
            driverSelection.Add("Network Drives");
            driverSelection.Add("Removable Drives");

            foreach (DriveInfo drive in drives)
            {
                driverSelection.Add(drive.Name);
            }
        }

        private List<DriveInfo> CreateDriveList()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            List<DriveInfo> driveList = new List<DriveInfo>();

            switch (DriveSelection.SelectedItem.ToString())
            {
                case "All Drives":
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.DriveType == DriveType.Fixed || drive.DriveType == DriveType.Removable || drive.DriveType == DriveType.Network)
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
                case "Hard Drives":
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.DriveType == DriveType.Fixed)
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
                case "Hard Drives + Removables":
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.DriveType == DriveType.Fixed || drive.DriveType == DriveType.Removable)
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
                case "Network Drives":
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.DriveType == DriveType.Network)
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
                case "Removable Drives":
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.DriveType == DriveType.Removable)
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
                default:
                    foreach (DriveInfo drive in drives)
                    {
                        if (drive.Name == DriveSelection.SelectedItem.ToString())
                        {
                            driveList.Add(drive);
                        }
                    }
                    break;
            }
            return driveList;
        }

        private void StartIndexerButton_Click(object sender, RoutedEventArgs e)
        {
            treeBuilderThreads.Add(new Thread(Builder));
            drives = CreateDriveList();
            treeBuilderThreads[treeBuilderThreads.Count - 1].Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            active = false;
        }

    }
}
